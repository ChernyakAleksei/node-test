 document.addEventListener('DOMContentLoaded',function () {
   const el = document.querySelector('#linkSearch');
   el.addEventListener('keyup', function (e) {
     e.preventDefault();
     const key = e.which || e.keyCode;
     if (key === 13) { 
       window.location = `http://localhost:3001/new/?url=${this.value}`;
     }
   });
 });

