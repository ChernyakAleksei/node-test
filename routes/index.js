const express = require('express');
const cheerio = require('cheerio');
const router = express.Router();
const request = require('request');
// let URL = 'rs/';
/* GET home page. */

router.get('/', function (req, res) {
  const imgsrcs = [];
  res.render('index', { title: 'site', srcArr: imgsrcs });
});

router.get('/:name', function (req, res) {
  let imgsrc;
  const URL = req.query.url;
  request(URL, function (error, response, body) {
    if (!error && response.statusCode == 200) {
      let $ = cheerio.load(body);
      const imgsrcs=[];
      for (let i=0;i < 15;i++){
        let srcimg = $('img').eq(i).attr('src');
        let src = srcimg.startsWith('http') ? srcimg : URL + srcimg;
        imgsrcs.push(src);
      }  
      res.render('index', { title: 'Next site', srcArr: imgsrcs });
    }
  });
});


module.exports = router;
