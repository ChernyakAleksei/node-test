const express = require('express');
const router = express.Router();
const cards = require('./bd.json');

class DataBase {

  constructor(data) {
    this.db = data;
  }
  get database() {
    return this.db;
  }
  join(nweItem, key) {
    if (nweItem.id) {
      const sameId = this.db[key].filter(item => item.id === nweItem.id);
      (sameId.length > 0) ? nweItem.id = +this.db[key][this.db[key].length - 1].id + 1 : nweItem.id;
    } else {
      nweItem.id = +this.db[key][this.db[key].length - 1].id + 1;
    }
    this.db[key].push(nweItem);
  }
  put(nweItem, key) {
    nweItem.id = +nweItem.id;
    this.db[key].forEach((item, i) => {
      (+item.id) === nweItem.id ? Object.assign(this.db[key][i], nweItem) : item; });
  }
  del(id, key) {
    this.db[key] = this.db[key].filter(item => +item.id != id);
  }
}

const Db = new DataBase(cards);

/* GET users listing. */

router.get('/:name', function (req, res) {
  const reqObj = Object.keys(req.query);
  let Obj;
  if (reqObj.length > 0) {
    Obj = Db.database[req.params.name].filter(item => +item[reqObj[0]] == +req.query[reqObj[0]]);
  } else {
    Obj = Db.database[req.params.name];
  }
  res.status('200').json(Obj);
});

router.get('/', function (req, res) {
  res.status('200').json(Db.database);
});

router.post('/:name', function (req, res) {
  Db.join(req.body, req.params.name);
  res.status('201').json(Db.database);
});

router.put('/:name', function (req, res) {
  Db.put(req.body, req.params.name);
  res.status('201').json(Db.database);
});

router.delete('/:name/:id', function (req, res) {
  console.log(req.params.id);
  console.log(req.params.name);
  Db.del(req.params.id, req.params.name);
  res.status('200').end();
});

module.exports = router;
